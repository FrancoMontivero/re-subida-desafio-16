"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = require("knex");
class Message {
    constructor(client, filename) {
        this.knex = knex_1.knex({ client, connection: { filename }, useNullAsDefault: true });
    }
    generateTable() {
        return new Promise((resolve, reject) => {
            this.knex.schema.hasTable('mensajes')
                .then((exists) => {
                if (!exists) {
                    this.knex.schema.createTable("mensajes", function (table) {
                        table.increments("id");
                        table.string("from");
                        table.string("message");
                        table.string("hour");
                        table.string("date");
                    })
                        .then(() => { resolve(null); })
                        .catch((err) => { reject(err); });
                }
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
    generateSyncTable() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const exists = yield this.knex.schema.hasTable("mensajes");
                if (!exists) {
                    yield this.knex.schema.createTable('mensajes', function (table) {
                        table.increments("id");
                        table.string("from");
                        table.string("message");
                        table.string("hour");
                        table.string("date");
                    });
                }
            }
            catch (err) {
                throw err;
            }
        });
    }
    selectMessages() {
        return new Promise((resolve, reject) => {
            this.knex("mensajes").from("mensajes").select("*")
                .then((rows) => {
                resolve(rows);
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
    selectSyncMessages() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rows = yield this.knex("mensajes").from("mensajes").select("*");
                return rows;
            }
            catch (err) {
                throw err;
            }
        });
    }
    addMessage(message) {
        return this.knex("mensajes").insert(message);
    }
    addSyncMessage(message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.knex("mensajes").insert(message);
            }
            catch (err) {
                throw err;
            }
        });
    }
}
exports.default = Message;

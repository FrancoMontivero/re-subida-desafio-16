"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = require("knex");
class Message {
    constructor(client, host, user, password, database) {
        this.knex = knex_1.knex({
            client,
            connection: { host, user, password, database }
        });
    }
    generateTable() {
        return new Promise((resolve, reject) => {
            this.knex.schema.hasTable('productos')
                .then((exists) => {
                if (!exists) {
                    this.knex.schema.createTable("productos", function (table) {
                        table.increments("id");
                        table.string("title");
                        table.decimal("price");
                        table.string("thumbnail");
                    })
                        .then(() => { resolve(null); })
                        .catch((err) => { reject(err); });
                }
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
    generateSyncTable() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const exists = yield this.knex.schema.hasTable("productos");
                if (!exists) {
                    yield this.knex.schema.createTable('productos', function (table) {
                        table.increments("id");
                        table.string("title");
                        table.decimal("price");
                        table.string("thumbnail");
                    });
                }
            }
            catch (err) {
                throw err;
            }
        });
    }
    selectProducts() {
        return new Promise((resolve, reject) => {
            this.knex("productos").from("productos").select("*")
                .then((rows) => {
                resolve(rows);
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
    selectSyncProducts() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rows = yield this.knex("productos").from("productos").select("*");
                return rows;
            }
            catch (err) {
                throw err;
            }
        });
    }
    selectProductById(id) {
        return this.knex("productos").from("productos").select("*").where("id", "=", `${id}`);
    }
    selectSyncProductById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const product = yield this.knex("productos").from("productos").select("*").where("id", "=", `${id}`);
                return product;
            }
            catch (err) {
                throw err;
            }
        });
    }
    addProduct(product) {
        return this.knex("productos").insert(product);
    }
    addSyncProduct(product) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.knex("productos").insert(product);
            }
            catch (err) {
                throw err;
            }
        });
    }
    updateProduct(id, data) {
        return this.knex("productos").from("productos").where("id", "=", `${id}`).update(data);
    }
    updateSyncProduct(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.knex("productos").from("productos").where("id", "=", `${id}`).update(data);
            }
            catch (err) {
                throw err;
            }
        });
    }
    removeProduct(id) {
        return this.knex("productos").from("productos").where("id", "=", `${id}`).del();
    }
    removeSyncProduct(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.knex("productos").from("productos").where("id", "=", `${id}`).del();
            }
            catch (err) {
                throw err;
            }
        });
    }
}
exports.default = Message;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.emitNewMessage = exports.emitMessages = exports.emitProducts = exports.initServer = void 0;
const products_1 = require("./products");
const messages_1 = require("./messages");
let refIo;
function initServer(io) {
    refIo = io;
    io.on("connection", (socket) => __awaiter(this, void 0, void 0, function* () {
        emitProducts(yield products_1.getProducts());
        emitMessages(yield messages_1.getMessages());
        socket.on("new-message", (data) => __awaiter(this, void 0, void 0, function* () {
            const newMessage = yield messages_1.addMessage(new Date(data.date), data.message, data.from);
            if (newMessage)
                emitNewMessage(newMessage);
        }));
    }));
}
exports.initServer = initServer;
function emitProducts(products) {
    refIo.emit("products", products);
}
exports.emitProducts = emitProducts;
;
function emitMessages(messages) {
    return __awaiter(this, void 0, void 0, function* () {
        refIo.emit("messages", messages);
    });
}
exports.emitMessages = emitMessages;
function emitNewMessage(message) {
    refIo.emit("new-message", message);
}
exports.emitNewMessage = emitNewMessage;

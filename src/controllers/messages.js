"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addMessage = exports.getMessages = void 0;
const path_1 = __importDefault(require("path"));
const Message_1 = __importDefault(require("../models/Message"));
const config_1 = require("../config");
const knexMessage = new Message_1.default(config_1.DB_MESSAGE_CLIENT, path_1.default.resolve(__dirname, '../', '../', "DB", config_1.DB_MESSAGE_FILENAME));
(function () {
    return __awaiter(this, void 0, void 0, function* () { yield knexMessage.generateSyncTable(); });
})();
// .then(() => console.log("Table Mensajes created in database Mensajes"))
// .catch(err => console.log(`${err.message} in create table Mensajes in database Mensajes`))
function getMessages() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let aux = {};
            const messages = yield knexMessage.selectSyncMessages();
            for (const message of messages) {
                if (aux.hasOwnProperty(message.date)) {
                    aux[message.date].push({ from: message.from, message: message.message, hour: message.hour });
                }
                else {
                    aux[message.date] = [{ from: message.from, message: message.message, hour: message.hour }];
                }
            }
            return aux;
        }
        catch (err) {
            return {};
        }
    });
}
exports.getMessages = getMessages;
function addMessage(fullDate, message, from) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newMessage = {
                from,
                message,
                hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`,
                date: fullDate.toString().split(' ').slice(0, 4).join(" ")
            };
            yield knexMessage.addSyncMessage(newMessage);
            return { [newMessage.date]: { from: newMessage.from, message: newMessage.message, hour: newMessage.hour } };
        }
        catch (err) {
            return null;
        }
    });
}
exports.addMessage = addMessage;

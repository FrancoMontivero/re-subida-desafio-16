"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeProduct = exports.updateProduct = exports.addProduct = exports.getProduct = exports.getProducts = void 0;
const socket_1 = require("./socket");
const Product_1 = __importDefault(require("../models/Product"));
const config_1 = require("../config");
const knexProduct = new Product_1.default(config_1.DB_PRODUCTS_CLIENT, config_1.DB_PRODUCTS_HOST, config_1.DB_PRODUCTS_USER, config_1.DB_PRODUCTS_PASSWORD, config_1.DB_PRODUCTS_DATABASE);
(function () {
    return __awaiter(this, void 0, void 0, function* () { yield knexProduct.generateSyncTable(); });
})();
function getProducts() {
    return __awaiter(this, void 0, void 0, function* () {
        const products = yield knexProduct.selectSyncProducts();
        return products;
    });
}
exports.getProducts = getProducts;
;
function getProduct(id) {
    return __awaiter(this, void 0, void 0, function* () {
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        const product = knexProduct.selectSyncProductById(id);
        if (product)
            return product;
        else
            throw new Error("Producto no encontrado");
    });
}
exports.getProduct = getProduct;
function addProduct(title, price, thumbnail) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(title && price && thumbnail))
            throw new Error("Hay parametros vacios o indefinidos");
        if (Number.isNaN(Number(price)))
            throw new Error("El precio no puede contener caracteres no numericos");
        else if (typeof price !== "number")
            price = parseFloat(price);
        const newProduct = { title, price, thumbnail };
        yield knexProduct.addSyncProduct(newProduct);
        try {
            const products = yield knexProduct.selectSyncProducts();
            socket_1.emitProducts(products);
        }
        finally {
            return newProduct;
        }
    });
}
exports.addProduct = addProduct;
function updateProduct(id, title, price, thumbnail) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(title && price && thumbnail))
            throw new Error("Hay parametros vacios o indefinidos");
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        if (Number.isNaN(Number(price)))
            throw new Error("El precio no puede contener caracteres no numericos");
        else if (typeof price !== "number")
            price = parseFloat(price);
        const product = yield knexProduct.selectSyncProductById(id);
        if (!product)
            throw new Error(`No existe un producto con el id ${id}`);
        const newProduct = { title, price, thumbnail };
        try {
            yield knexProduct.updateSyncProduct(id, newProduct);
            const products = yield knexProduct.selectSyncProducts();
            socket_1.emitProducts(products);
        }
        catch (err) {
            throw err;
        }
        finally {
            return product;
        }
    });
}
exports.updateProduct = updateProduct;
function removeProduct(id) {
    return __awaiter(this, void 0, void 0, function* () {
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        const product = yield knexProduct.selectSyncProductById(id);
        if (!product)
            throw new Error(`No existe un producto con el id ${id}`);
        try {
            yield knexProduct.removeSyncProduct(id);
            const products = yield knexProduct.selectSyncProducts();
            socket_1.emitProducts(products);
        }
        finally {
            return product;
        }
    });
}
exports.removeProduct = removeProduct;

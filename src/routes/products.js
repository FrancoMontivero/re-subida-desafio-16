"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const products_1 = require("../controllers/products");
const routerProducts = express_1.default.Router();
routerProducts.get('/listar', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const products = yield products_1.getProducts();
        if (products.length === 0)
            res.json({ error: "No hay productos cargados" });
        else
            res.json(products);
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
}));
routerProducts.get('/listar/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const product = yield products_1.getProduct(parseInt(req.params.id));
        res.json(product);
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
}));
routerProducts.post('/guardar', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { title, price, thumbnail } = req.body;
    try {
        res.json(yield products_1.addProduct(title, price, thumbnail));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
}));
routerProducts.put('/actualizar/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { title, price, thumbnail } = req.body;
    const id = parseInt(req.params.id);
    try {
        res.json(yield products_1.updateProduct(id, title, price, thumbnail));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
}));
routerProducts.delete('/borrar/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    try {
        res.json(yield products_1.removeProduct(id));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
}));
exports.default = routerProducts;

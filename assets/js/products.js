const socket = io();

function main() {
    const source = document.querySelector('#listing').innerHTML;
    const template = Handlebars.compile(source);

    socket.on('products', products => {
        document.querySelector("#listing-products").innerHTML = template({products});
    })
}

document.querySelector('#form').onsubmit = e => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    fetch(`${window.location.origin}/api/productos/guardar`, { method: "POST", body: formData })
    .then(data => data.json())
    .then(data => {
        for(children of document.querySelector('#form').children) {
            children.value = '';
        }
        if(data.hasOwnProperty('error')){
            const selectorAlertFailed = document.querySelector('#alert-failed');
            selectorAlertFailed.classList.remove('d-none');
            selectorAlertFailed.innerHTML = data.error;
            setTimeout(() => { selectorAlertFailed.classList.add('d-none'); }, 3000)
        }else {
            const selectorAlertSuccess = document.querySelector('#alert-success');
            selectorAlertSuccess.classList.remove('d-none');
            setTimeout(() => { selectorAlertSuccess.classList.add('d-none'); }, 3000)
        }
    })         
    .catch(err => {
        const selectorAlertFailed = document.querySelector('#alert-failed');
        selectorAlertFailed.classList.remove('d-none');
        selectorAlertFailed.innerHTML = 'Parece que hubo un error';
        setTimeout(() => { selectorAlertFailed.classList.add('d-none'); }, 3000)
    });
}

main();
var maxScrollTop;
var scrollHeight;
var email;

function onSubmit(e) {
    e.preventDefault();
    const message = e.target.children[0].value;
    e.target.children[0].value = "";
    const dataMessage = {date: Date.now(), message, from: email}
    io().emit("new-message", dataMessage);
};

function createMessage(message) {
    return  '<li class="message">' +
                '<div class="message__from-container">' +
                    '<span class="message__from">' + message.from + '</span>' +
                '</div>' +
                '<div class="message__text-container">' +
                    '<span class="message__text">' + message.message + '</span>' +
                '</div>' +
                '<div class="message__hour-container">' +
                    '<span class="message__hour">' + message.hour + '</span>' +
                '</div>' +
            '</li>'
}

function createListMessage(messages) {
    let aux = ''
    for(date in messages) {
        let msg = '<div class="messages__date">' +
                    '<span>' + date + '</span>' +
                  '</div>';
        for(message of messages[date]) {
            msg += createMessage(message);
        }
        aux += '<ol class="messages-container">' + msg + '</ol>';
    }
    return aux;
}

function initMessages(messages) {
    const messageContainerSelector = document.querySelector(".messages");
    if(Object.keys(messages).length === 0) {
        messageContainerSelector.innerHTML = '<div class="w-100 h-100 d-flex align-items-center justify-content-center">' + 
                                                '<h2 class="text-center text-white" id="alert-chat-empty">' +
                                                    'El chat esta vacio' + 
                                                '</h2>' +
                                            '</div>'
    } else {
        messageContainerSelector.innerHTML = createListMessage(messages);
    }
    messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
    maxScrollTop = messageContainerSelector.scrollTop;
    scrollHeight = messageContainerSelector.scrollHeight;
}

function addMessage(message) {
    const messageContainerSelector = document.querySelector(".messages");
    const date = Object.keys(message)[0];
    let isUploadedDate = false;
    let i = 0;
    if(!document.querySelector("#alert-chat-empty")){
        while(!isUploadedDate && i < messageContainerSelector.children.length) {
            if(messageContainerSelector.children[i].children[0].children[0].innerText === date) {
                messageContainerSelector.children[i].innerHTML += createMessage(message[date]);
                if(maxScrollTop <= 300 || messageContainerSelector.scrollTop >= maxScrollTop - 300) {
                    messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
                }
                maxScrollTop += messageContainerSelector.scrollHeight - scrollHeight;
                scrollHeight = messageContainerSelector.scrollHeight;
                isUploadedDate = true;
            }
            i++;
        }
    }else messageContainerSelector.innerHTML = "";
    if(!isUploadedDate) {
        messageContainerSelector.innerHTML += '<ol class="messages-container">' +
                                                '<div class="messages__date">' +
                                                    '<span>' + date + '</span>' +
                                                    '</div>' + 
                                                    createMessage(message[date]) + 
                                              '</ol>'
    }
}

function configureScroll() {
    const buttonScrollBottomSelector = document.querySelector("#btn-scroll-bottom");
    const messageContainerSelector = document.querySelector(".messages");
    let prevScroll = messageContainerSelector.scrollTop
    messageContainerSelector.onscroll = e => {
        if(e.target.scrollTop > prevScroll && e.target.scrollTop <= maxScrollTop - 200) {
            buttonScrollBottomSelector.style.display = "block";
        } else buttonScrollBottomSelector.style.display = "none";
        prevScroll = messageContainerSelector.scrollTop;
    };
}

function configureButtonScrollBottom() {
    const messageContainerSelector = document.querySelector(".messages");
    document.querySelector("#btn-scroll-bottom").onclick = e => {
        messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
    }
}

function activeChat() {
    document.querySelector(".chat__input").onsubmit = onSubmit;

    io().on("messages", messages => {
        initMessages(messages);
    });
    
    io().on("new-message", message => {
        addMessage(message);
    });

    configureScroll();
    configureButtonScrollBottom();
}

function main() {
    const formEmailSelector = document.querySelector(".form-email");
    formEmailSelector.addEventListener('submit', function(e) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        email = formData.get('email');
        e.currentTarget.remove();
        activeChat();
    })
}

main();
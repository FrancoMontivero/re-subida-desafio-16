import { knex } from 'knex'
import { ProductInterface }  from '../controllers/products';

export default class Message {
    private knex: any;

    constructor(client: string, host: string, user: string, password: string, database: string) {
        this.knex = knex({
            client, 
            connection: { host,user,password,database } 
        });
    }

    generateTable(): Promise<any> {
        return new Promise((resolve, reject) => {
           this.knex.schema.hasTable('productos')
            .then((exists: any) => {
                if(!exists) {
                    this.knex.schema.createTable("productos", function(table: any) {
                        table.increments("id");
                        table.string("title");
                        table.decimal("price");
                        table.string("thumbnail")
                    })
                    .then(() => { resolve(null) } )
                    .catch((err: any) => { reject(err) })
                }
            })
            .catch((err: any)  => {
                reject(err);
            })

        })
    }

    async generateSyncTable(): Promise<any> {
        try {
            const exists = await this.knex.schema.hasTable("productos")
            if(!exists) {
                await this.knex.schema.createTable('productos', function(table: any) {
                    table.increments("id");
                    table.string("title");
                    table.decimal("price");
                    table.string("thumbnail")
                })
            }
        }
        catch(err) { throw err; }
    }
    
    selectProducts(): Promise<Array<ProductInterface> | Error> {
        return new Promise((resolve, reject) => {
            this.knex("productos").from("productos").select("*")
            .then((rows: Array<ProductInterface>) => {
                resolve(rows);
            })
            .catch((err: any) => {
                reject(err);
            })
        })
    }

    async selectSyncProducts(): Promise<Array<ProductInterface>> {
        try { 
            const rows: Array<ProductInterface> = await this.knex("productos").from("productos").select("*") 
            return rows;
        }
        catch(err) { throw err; }
    }

    
    selectProductById(id: number): Promise<ProductInterface> {
        return this.knex("productos").from("productos").select("*").where("id", "=", `${id}`);
    
    }

    async selectSyncProductById(id: number): Promise<ProductInterface> {
        try {
            const product = await this.knex("productos").from("productos").select("*").where("id", "=", `${id}`)
            return product;
        }
        catch (err) { throw err; }
    }

    addProduct(product: ProductInterface) {
        return this.knex("productos").insert(product)
    }

    async addSyncProduct(product: ProductInterface) {
        try { await this.knex("productos").insert(product) }
        catch(err) { throw err; }
    }

    updateProduct(id: number, data: ProductInterface) {
        return this.knex("productos").from("productos").where("id", "=", `${id}`).update(data);
    }

    async updateSyncProduct(id: number, data: ProductInterface) {
        try {
            await this.knex("productos").from("productos").where("id", "=", `${id}`).update(data);            
        }
        catch (err) {
            throw err;
        }
    }

    removeProduct(id: number) {
        return this.knex("productos").from("productos").where("id", "=", `${id}`).del();
    }

    async removeSyncProduct(id: number) {
        try {
            await this.knex("productos").from("productos").where("id", "=", `${id}`).del();            
        }
        catch (err) {
            throw err;
        }
    }
}
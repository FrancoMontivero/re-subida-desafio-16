import { knex } from 'knex'
import { MessageInterface }  from '../controllers/messages';

export default class Message {
    private knex: any;

    constructor(client: string, filename: string) {
        this.knex = knex({client, connection: { filename }, useNullAsDefault: true });
    }

    generateTable(): Promise<any> {
        return new Promise((resolve, reject) => {
           this.knex.schema.hasTable('mensajes')
            .then((exists: any) => {
                if(!exists) {
                    this.knex.schema.createTable("mensajes", function(table: any) {
                        table.increments("id");
                        table.string("from");
                        table.string("message");
                        table.string("hour");
                        table.string("date");
                    })
                    .then(() => { resolve(null) } )
                    .catch((err: any) => { reject(err) })
                }
            })
            .catch((err: any)  => {
                reject(err);
            })

        })
    }

    async generateSyncTable(): Promise<any> {
        try {
            const exists = await this.knex.schema.hasTable("mensajes")
            if(!exists) {
                await this.knex.schema.createTable('mensajes', function(table: any) {
                    table.increments("id");
                    table.string("from");
                    table.string("message");
                    table.string("hour");
                    table.string("date");
                })
            }
        }
        catch(err) { throw err; }
    }
    
    selectMessages(): Promise<Array<MessageInterface> | Error> {
        return new Promise((resolve, reject) => {
            this.knex("mensajes").from("mensajes").select("*")
            .then((rows: Array<MessageInterface>) => {
                resolve(rows);
            })
            .catch((err: any) => {
                reject(err);
            })
        })
    }

    async selectSyncMessages(): Promise<Array<MessageInterface>> {
        try { 
            const rows: Array<MessageInterface> = await this.knex("mensajes").from("mensajes").select("*") 
            return rows;
        }
        catch(err) { throw err; }
    }

    addMessage(message: MessageInterface) {
        return this.knex("mensajes").insert(message)
    }

    async addSyncMessage(message: MessageInterface) {
        try { await this.knex("mensajes").insert(message) }
        catch(err) { throw err; }
    }
}
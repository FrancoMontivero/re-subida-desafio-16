import dotenv from 'dotenv';
import path from 'path';

dotenv.config({path: path.resolve(__dirname, '../', process.env.NODE_ENV + '.env')});

export let PORT: number = process.env.PORT ? parseInt(process.env.PORT) : 8080;
export let ORIGIN_CLIENT: string = process.env.ORIGIN_CLIENT || 'http://localhost:8081';
export let SHOW_REQUEST: string = process.env.SHOW_REQUEST || 'enabled';
export let DB_MESSAGE_CLIENT: string = 'sqlite3';
export let DB_MESSAGE_FILENAME: string = "mensajes.sqlite";
export let DB_PRODUCTS_CLIENT: string = "mysql";
export let DB_PRODUCTS_HOST: string = "127.0.0.1";
export let DB_PRODUCTS_USER: string = "root";
export let DB_PRODUCTS_PASSWORD: string = "";
export let DB_PRODUCTS_DATABASE: string = "productos";
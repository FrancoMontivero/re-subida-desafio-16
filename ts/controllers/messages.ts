import path from 'path';
import Message from '../models/Message';

import { DB_MESSAGE_FILENAME, DB_MESSAGE_CLIENT } from '../config';

const knexMessage = new Message(DB_MESSAGE_CLIENT, path.resolve(__dirname, '../', '../', "DB", DB_MESSAGE_FILENAME));

export interface MessageInterface {
    from: string,
    message: string,
    hour: string,
    date: string
}

(async function () { await knexMessage.generateSyncTable() } )()
// .then(() => console.log("Table Mensajes created in database Mensajes"))
// .catch(err => console.log(`${err.message} in create table Mensajes in database Mensajes`))

export async function getMessages(): Promise<{[key: string]: [{from: string, message: string, hour: string}]}> {
    try {
        let aux: {[key: string]: [{from: string, message: string, hour: string}]} = {};
        const messages: Array<MessageInterface> = await knexMessage.selectSyncMessages();
        for(const message of messages) {
            if(aux.hasOwnProperty(message.date)) {
                aux[message.date].push({from: message.from, message: message.message, hour: message.hour});
            } else {
                aux[message.date] = [{from: message.from, message: message.message, hour: message.hour}];
            }
        }
        return aux;
    } catch (err) {
        return {} 
    }
}

export async function addMessage(fullDate: Date, message: string, from: string): Promise<{[key: string]: {from: string, message: string, hour: string}} | null> {
    try {
        const newMessage: MessageInterface = {
            from, 
            message, 
            hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`,
            date: fullDate.toString().split(' ').slice(0, 4).join(" ")
        };
        await knexMessage.addSyncMessage(newMessage);
        return {[newMessage.date]: {from: newMessage.from, message: newMessage.message, hour: newMessage.hour}};
    } catch (err) {
        return null;
    }
}
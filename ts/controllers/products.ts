import { emitProducts } from './socket';
import Product from '../models/Product';

import { DB_PRODUCTS_CLIENT, DB_PRODUCTS_HOST, DB_PRODUCTS_USER, DB_PRODUCTS_PASSWORD, DB_PRODUCTS_DATABASE } from '../config';

const knexProduct = new Product(DB_PRODUCTS_CLIENT, DB_PRODUCTS_HOST, DB_PRODUCTS_USER, DB_PRODUCTS_PASSWORD, DB_PRODUCTS_DATABASE);

export interface ProductInterface {
    id?: number,
    title: string,
    price: number,
    thumbnail: string
}

(async function () { await knexProduct.generateSyncTable() } )()

export async function getProducts(): Promise<Array<ProductInterface>> { 
    const products = await knexProduct.selectSyncProducts();
    return products;
};

export async function getProduct(id: number | string): Promise<Object | Product> {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const product = knexProduct.selectSyncProductById(id);
    if(product) return product
    else throw new Error("Producto no encontrado");
}

export async function addProduct(title: string, price: string | number, thumbnail: string): Promise<ProductInterface> {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 

    const newProduct = { title, price, thumbnail };
    await knexProduct.addSyncProduct(newProduct);
    try { 
        const products = await knexProduct.selectSyncProducts();
        emitProducts(products); 
    }
    finally { return newProduct }
}

export async function updateProduct(id: number | string, title: string, price: number, thumbnail: string): Promise<ProductInterface> {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico");
    else if(typeof id !== "number") id = parseInt(id);
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 
    const product = await knexProduct.selectSyncProductById(id);
    if(!product) throw new Error(`No existe un producto con el id ${id}`);
    const newProduct = { title, price, thumbnail };
    try { 
        await knexProduct.updateSyncProduct(id, newProduct);
        const products = await knexProduct.selectSyncProducts();
        emitProducts(products); 
    }
    catch(err) { throw err; }
    finally {
        return product; 
    }
}

export async function removeProduct(id: number | string) {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const product = await knexProduct.selectSyncProductById(id);
    if(!product) throw new Error(`No existe un producto con el id ${id}`);
    try { 
        await knexProduct.removeSyncProduct(id);
        const products = await knexProduct.selectSyncProducts();
        emitProducts(products); 
    }
    finally { return product; }
}
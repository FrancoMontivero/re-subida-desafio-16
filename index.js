const morgan = require('morgan');
const { PORT } = require('./src/config');
const app = require('./src/app')['default'];

app.listen(PORT, () => {
    console.log(`listen in the port ${PORT}`);
})